package com.karzoun;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

@Path("movies")
public class MovieResource{
    public static List<String> MOVIES = new ArrayList<>();
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response all(){
        return Response.ok(MOVIES).build();
    }
    @GET
    @Path(("/size"))
    @Produces(MediaType.TEXT_PLAIN)
    public Response countMovies(){
        return Response.ok(MOVIES.size()).build();
    }
    
    @POST
    @Path(("/create"))
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.TEXT_PLAIN)
    public Response countMovies(String name){
        MOVIES.add(name);
        return Response
                .status(Response.Status.CREATED)
                .entity(MOVIES)
                .build();
    }
}
